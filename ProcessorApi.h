#pragma once
#include "ProcessorCalibration.h"
#include "ProcessorWrapper.h"
class ProcessorApi
{
private:
	ProcessorCalibration* prcsrCali;
	ProcessorWrapper* octPrcssr;
	void setPrames();
public:
	ProcessorApi();
	~ProcessorApi();

	// add by tao @2017 04 06: to crop the raw data being in the range of [cameraStartPixel, cameraEndPixel]
	void setProcessorCalibration(int batchFrames, int batchAscans, int prevFreq, bool reslice,
		int numAscansPerBscan, int numBScans, int avgBscan, int numCameraPixels, int numBgFrame,
		int cameraStartPixel, int cameraEndPixel, int numUsedPixels,
		int startPixel, int endPixel, int grayLevel, bool interpLinear,
		bool dispModeGS, int dispFrame, int dispTests, float dispA2, float dispA3,
		std::string fnamePhase, std::string fnameBg, std::string fnameData,
		int contrastA, int contrastB);

	void resetDispersion(bool dispModeGS, int dispFrame, int dispTests, float dispA2, float dispA3);

	bool processBg();
	bool processBg(int dispModeGS, float &a2, float &a3);
	bool processFrameData(std::string fnameData2, std::string datatype);
};

