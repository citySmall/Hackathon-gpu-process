==12064== Profiling application: ./GPU_process
==12064== Profiling result:
Time(%)      Time     Calls       Avg       Min       Max  Name
 14.68%  422.83ms      1406  300.73us  12.385us  684.20us  void spVector8192D::kernelTex<unsigned int, float, fftDirection_t=1, unsigned int=1, unsigned int=1, ARITHMETIC, ALL, WRITEBACK>(kernel_parameters_t<fft_tex_t, unsigned int, float>)
 11.73%  337.93ms      1203  280.90us     320ns  3.2116ms  [CUDA memcpy DtoH]
 10.88%  313.31ms       603  519.58us  1.6320us  1.8181ms  linear_interp(int, int, float*, float2*, float2*)
  8.98%  258.79ms       802  322.68us  188.07us  404.71us  phi_multiply(int, int, float2*, float2*, float2*)
  8.48%  244.38ms       809  302.07us     992ns  2.0778ms  [CUDA memcpy HtoD]
  7.92%  228.05ms       601  379.45us  358.53us  517.03us  mult_divide(int, int, float*, float2*, float2*, float2*)
  7.72%  222.43ms       806  275.97us  50.913us  343.56us  scale_IFT(int, int, float, float2*)
  6.61%  190.57ms      1207  157.89us     385ns  220.07us  [CUDA memset]
  6.46%  186.18ms       600  310.30us  307.36us  343.01us  scale_IFT_x(int, int, float, float2*)
  4.02%  115.74ms       600  192.91us  189.09us  215.52us  subtract(int, int, unsigned short*, float*, float2*)
  3.48%  100.30ms       603  166.34us  5.7280us  184.00us  void spRadix0032B::kernel1Mem<unsigned int, float, fftDirection_t=-1, unsigned int=64, unsigned int=4, CONSTANT, ALL, WRITEBACK>(kernel_parameters_t<fft_mem_radix1_t, unsigned int, float>)
  3.31%  95.284ms       603  158.02us  4.4800us  183.14us  void spRadix0125C::kernel3Mem<unsigned int, float, fftDirection_t=-1, unsigned int=16, unsigned int=3, CONSTANT, ALL, WRITEBACK>(kernel_parameters_t<fft_mem_radix3_t, unsigned int, float>)
  1.97%  56.705ms       603  94.037us  1.3440us  96.097us  zero_pad(int, int, int, float2*, float2*)
  1.06%  30.426ms       600  50.710us  50.080us  51.425us  multiframe_transpose(int, int, int, float*, float*)
  0.94%  27.142ms       802  33.842us  25.185us  84.609us  trim_width(int, int, int, int, float2*, float2*)
  0.62%  17.761ms       603  29.454us  28.320us  234.40us  magnitude_db(int, int, float2*, float*)
  0.52%  14.867ms       600  24.777us  23.424us  26.848us  subt_divide(int, int, float*, float*, float, float*)
  0.17%  4.8106ms      1200  4.0080us  1.7600us  8.7040us  d_sum_elements(int, int, float*, float*)
  0.11%  3.2044ms      1600  2.0020us     864ns  3.9360us  [CUDA memcpy DtoD]
  0.11%  3.1083ms       202  15.387us  14.080us  111.17us  transpose(int, int, float*, float*)
  0.08%  2.2771ms         1  2.2771ms  2.2771ms  2.2771ms  trim_height(int, int, int, int, float2*, float2*)
  0.04%  1.0664ms       200  5.3320us  4.7040us  10.304us  magnitude(int, int, float2*, float*)
  0.03%  906.47us       200  4.5320us  4.3200us  6.4000us  divide(int, int, float*, float, float*)
  0.03%  892.24us       200  4.4610us  4.3520us  9.2800us  multiply(int, int, float*, float*, float*)
  0.03%  887.72us       200  4.4380us  4.1920us  4.6080us  d_log(int, int, float*, float*)
  0.02%  542.50us         2  271.25us  238.76us  303.75us  subtract(int, int, float2*, float*, float2*)
  0.02%  510.70us         2  255.35us  163.84us  346.85us  void spVector8192D::kernelTex<unsigned int, float, fftDirection_t=-1, unsigned int=1, unsigned int=1, ARITHMETIC, ALL, WRITEBACK>(kernel_parameters_t<fft_tex_t, unsigned int, float>)
  0.00%  14.880us         3  4.9600us  4.8320us  5.1200us  repmat(int, int, float*)
  0.00%  9.7280us         2  4.8640us  4.8320us  4.8960us  repmat(int, int, float2*)
  0.00%  4.4480us         1  4.4480us  4.4480us  4.4480us  interp_repmat(int, int, float*)
  0.00%  1.6960us         1  1.6960us  1.6960us  1.6960us  divide(int, int, float*, float, float2*)
  0.00%  1.5360us         1  1.5360us  1.5360us  1.5360us  reciprocal(int, int, float2*)

==12064== API calls:
Time(%)      Time     Calls       Avg       Min       Max  Name
 46.16%  2.26984s      1862  1.2190ms     407ns  2.17830s  cudaFree
 28.95%  1.42365s      3012  472.66us  4.8530us  9.3236ms  cudaMemcpy
 15.10%  742.39ms      1848  401.73us  6.0690us  567.01ms  cudaMalloc
  4.35%  213.81ms      9433  22.666us  3.5310us  2.0292ms  cudaLaunch
  1.96%  96.562ms      2614  36.940us  5.4320us  1.1167ms  cudaLaunchKernel
  0.92%  45.377ms      1207  37.594us  3.0880us  143.27us  cudaMemset
  0.57%  28.136ms       600  46.892us  16.387us  104.57us  cudaMemcpyAsync
  0.48%  23.648ms     45344     521ns     111ns  1.0191ms  cudaSetupArgument
  0.35%  17.215ms      1408  12.226us  2.2780us  65.649us  cudaBindTexture
  0.22%  10.923ms       603  18.113us  4.4200us  738.23us  cudaDeviceSynchronize
  0.21%  10.370ms      9433  1.0990us     124ns  26.209us  cudaConfigureCall
  0.19%  9.2280ms     11245     820ns     115ns  25.531us  cudaPeekAtLastError
  0.18%  8.7532ms        14  625.23us  145.18us  5.7681ms  cudaGetDeviceProperties
  0.11%  5.5867ms        10  558.67us  192.39us  757.25us  cudaMemGetInfo
  0.10%  4.6739ms      1408  3.3190us     684ns  66.554us  cudaUnbindTexture
  0.07%  3.3106ms      2614  1.2660us     168ns  1.0375ms  cudaGetLastError
  0.02%  1.0572ms         1  1.0572ms  1.0572ms  1.0572ms  cudaDeviceReset
  0.02%  1.0027ms         2  501.33us  176.11us  826.55us  cuDeviceTotalMem
  0.02%  763.01us       178  4.2860us     102ns  222.13us  cuDeviceGetAttribute
  0.01%  632.68us         1  632.68us  632.68us  632.68us  cudaHostUnregister
  0.01%  318.51us       416     765ns     294ns  18.429us  cudaFuncSetCacheConfig
  0.01%  302.72us         1  302.72us  302.72us  302.72us  cudaHostRegister
  0.00%  83.585us        98     852ns     214ns  9.8660us  cudaGetDevice
  0.00%  83.219us         2  41.609us  19.202us  64.017us  cuDeviceGetName
  0.00%  39.240us         2  19.620us  14.374us  24.866us  cudaStreamCreate
  0.00%  24.883us         2  12.441us  5.8170us  19.066us  cudaStreamDestroy
  0.00%  5.8240us         4  1.4560us     244ns  4.1030us  cuDeviceGetCount
  0.00%  3.1350us         4     783ns     161ns  1.5880us  cuDeviceGet
  0.00%     599ns         1     599ns     599ns     599ns  cuInit
  0.00%     354ns         1     354ns     354ns     354ns  cuDriverGetVersion
