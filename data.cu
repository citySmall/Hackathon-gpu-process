#include "data.cuh"

using namespace std;

Data::Data(Parameters &p) : Helper(p)
{
	d_gauss_win = NULL;
	genGaussWin();
}

Data::~Data()
{
	if (d_gauss_win) gpuErrchk( cudaFree(d_gauss_win) );
}

void Data::genGaussWin()
{
	float temp;
	float *gauss_win = new float[width_2x];
    for (int i = 0; i < width_2x; i++)
	{
        temp = p.alpha * (((i+1.f) / width) - 1.f);
		temp *= temp;
        gauss_win[i] = expf(temp*(-0.5f));
    }

	gpuErrchk( cudaMalloc((void **)&d_gauss_win, (p.batchAscans * width_2x * sizeof(float))) );
	gpuErrchk( cudaMemcpy(d_gauss_win, gauss_win, (width_2x * sizeof(float)), cudaMemcpyHostToDevice) );
	repmat<<<dimLine_w2,dimLine_B>>>(p.batchAscans, width_2x, d_gauss_win); gpuErrchk( cudaPeekAtLastError() );

	delete[] gauss_win;

	cout <<"	- Gauss win matrix created" << endl;
}

//load calibration file
void Data::loadFile(string fname, int length, float *result_array)
{
	std::cout<<__FUNCTION__<<" "<<fname.c_str()<<std::endl;
    ifstream is(fname.c_str());
	if (is)
	{
		for (int i = 0; i < length; i++)
		{
			is >> result_array[i];
			//std::cout<<result_array[i]<<"	";
		}
		//std::cout<<std::endl;
		is.close();
	}
	else
	{
		cerr << "File could not be opened!\n"; // Report error
		cerr << "Error code: " << strerror(errno); // Get some info as to why
		throw invalid_argument("Calibration File Error");
	}
}

/*
void Data::loadFile(string fname, int startLocation, int length, float2 *result_array)
{
	//std::cout<<__FUNCTION__<<"	"<<fname<<std::endl;
	//cout << __FUNCTION__ << " float2 1" << " startLocation " << startLocation << " length " << length << endl;
	ifstream is(fname, ios::in|ios::binary);
	if (is)
	{
		is.seekg(startLocation,ios::beg);
	
		int x = 0;
		for (int i = 0; i < length; ++i)
		{
			is.read(reinterpret_cast<char*>(&x), sizeof(uint16_t));
			result_array[i].x = (float)x;
			//result_array[i].y = 0;
		}
		is.close();
	}
	else
	{
		cerr << "File could not be opened!\n"; // Report error
		cerr << "Error code: " << strerror(errno); // Get some info as to why
		throw invalid_argument("File Open Error");
	}
}
*/

/*
void Data::loadFile(string fname, int startLocation, int length, uint16_t *result_array)
{
	//std::cout<<__FUNCTION__<<"	"<<fname<<std::endl;
	//cout << __FUNCTION__ << " uint16 1" << " startLocation " << startLocation << " length " << length << endl;
	length *=2;
	ifstream is(fname, ios::in|ios::binary);
	if (is)
	{
		is.seekg (startLocation, ios::beg);
		is.read(reinterpret_cast<char*>(result_array), length);
		is.close();
	}
	else
	{
		cerr << "File could not be opened!\n";
		cerr << "Error code: " << strerror(errno);
		throw invalid_argument("Data File Error");
	}
}
*/

// add by Brian
///get ROI load initial and middle
/*
void Data::loadFile(string fname, int startLocation, int all_pixels, int used_pixels, int height, float2 *result_array)
{
	//std::cout<<__FUNCTION__<<"	"<<fname<<std::endl;
	//cout << __FUNCTION__<< " startLocation " << startLocation << " all_pixels " << all_pixels << " used_pixels " << used_pixels<< " height " << height << endl;
	ifstream is(fname, ios::in|ios::binary);
	if (is)
	{
		int x = 0;
		for (int h = 0; h < height; h++)
		{
			//std::cout<<"position:	"<<startLocation + all_pixels * h * 2<<std::endl;
			is.seekg(startLocation + all_pixels * h * 2,ios::beg);
			for (int i = 0; i < used_pixels; i++)
			{
				is.read(reinterpret_cast<char*>(&x), sizeof(uint16_t));
				result_array[used_pixels * h + i].x = (float)x;
				//std::cout<<x<<"	";
			}
			//std::cout<<std::endl;
		}
		is.close();
	}
	else
	{
		cerr << "File could not be opened!\n"; // Report error
		cerr << "Error code: " << strerror(errno); // Get some info as to why
		throw invalid_argument("File Open Error");
	}
}
*/

//add by Tao
///get ROI load initial and middle
void Data::loadFile(string fname, unsigned long long startLocation, int all_pixels, int used_pixels, int height, float2 *result_array)
{
	//std::cout<<__FUNCTION__<<"	"<<fname<<std::endl;
	//cout << __FUNCTION__<< " startLocation " << startLocation << " all_pixels " << all_pixels << " used_pixels " << used_pixels<< " height " << height << endl;


	ifstream is(fname.c_str(), ios::in|ios::binary);
	if (is)
	{
		int x = 0;
		for (int h = 0; h < height; h++)
		{
			//std::cout<<"position:	"<<startLocation + all_pixels * h * 2<<std::endl;
			//is.seekg(startLocation + all_pixels * h * 2,ios::beg);
			is.seekg(startLocation + (all_pixels * h )* 2, ios::beg);
			for (int i = 0; i < used_pixels; i++)
			{
				is.read(reinterpret_cast<char*>(&x), sizeof(uint16_t));
				//result_array[used_pixels * h + i].x = (float)x;
				result_array[used_pixels * h + i].x = (float)x;
				//std::cout<<x<<"	";
			}
			//std::cout<<std::endl;
		}
		is.close();
	}
	else
	{
		cerr << fname << std::endl;
		cerr << "File could not be opened!\n"; // Report error
		cerr << "Error code: " << strerror(errno); // Get some info as to why
		throw invalid_argument("File Open Error");
	}
}



// add by Brian
//data.loadFile(fname, startLocation, all_pixels, used_pixels, height, result_array);
void Data::loadFile(string fname, int startLocation, int all_pixels, int used_pixels, int height, uint16_t *result_array)
{
	//std::cout<<__FUNCTION__<<"	"<<fname<<std::endl;
	//cout << __FUNCTION__<< " startLocation " << startLocation << " all_pixels " << all_pixels << " used_pixels " << used_pixels<< " height " << height << endl;

	ifstream is(fname.c_str(), ios::in|ios::binary);
	if (is)
	{
		for (int h = 0; h < height; h++)
		{
			//is.seekg(startLocation + all_pixels * h * 2,ios::beg);
			is.seekg(startLocation + (all_pixels * h ) * 2, ios::beg);
			//is.read(reinterpret_cast<char*>(result_array + used_pixels * h), used_pixels * 2);
			is.read(reinterpret_cast<char*>(result_array + used_pixels * h), used_pixels * 2);
		}
		is.close();   
	}
	else
	{
		cerr << fname << std::endl;
		cerr << "File could not be opened!\n";
		cerr << "Error code: " << strerror(errno);
		throw invalid_argument("Data File Error");
	}
}

// add by Brian
/*
void Data::loadFile(string fname, long long startLocation, int all_pixels, int used_pixels, int height, float2 *result_array)
{
	//std::cout<<__FUNCTION__<<"	"<<fname<<std::endl;
	//cout << __FUNCTION__<< " startLocation " << startLocation << " all_pixels " << all_pixels << " used_pixels " << used_pixels<< " height " << height << endl;
	ifstream is(fname, ios::in|ios::binary);
	if (is)
	{
		int x = 0;
		for (int h = 0; h < height; h++)
		{
			is.seekg(startLocation + all_pixels * h * 2,ios::beg);
			for (int i = 0; i < used_pixels; i++)
			{
				is.read(reinterpret_cast<char*>(&x), sizeof(uint16_t));
				result_array[used_pixels * h + i].x = (float)x;
			}
		}
		is.close();
	}
	else
	{
		cerr << "File could not be opened!\n"; // Report error
		cerr << "Error code: " << strerror(errno); // Get some info as to why
		throw invalid_argument("File Open Error");
	}
}*/

// add by Brian
//data.loadFile(fname, startLocation, all_pixels, used_pixels, height, result_array); for data process frame by frame
void Data::loadFile(string fname, long long startLocation, int all_pixels, int used_pixels, int height, uint16_t *result_array)
{
	//std::cout<<__FUNCTION__<<"	"<<fname<<std::endl;
	//cout << __FUNCTION__<< " startLocation " << startLocation << " all_pixels " << all_pixels << " used_pixels " << used_pixels<< " height " << height << endl;

	ifstream is(fname.c_str(), ios::in|ios::binary);
	if (is)
	{
		for (int h = 0; h < height; h++)
		{
			// is.seekg(startLocation + all_pixels * h * 2,ios::beg);
			is.seekg(startLocation + (all_pixels * h) * 2, ios::beg);
			//is.read(reinterpret_cast<char*>(result_array + used_pixels * h), used_pixels * 2);
			is.read(reinterpret_cast<char*>(result_array + used_pixels * h), used_pixels * 2);
		}
		is.close();
	}
	else
	{
		cerr << fname << std::endl;
		cerr << "File could not be opened!\n";
		cerr << "Error code: " << strerror(errno);
		throw invalid_argument("Data File Error");
	}
}



// the following are the need for setting camera start pixel and end pixel

void Data::loadFile(std::string fname, int startLocation, int all_pixels, int camStartPixel, int camEndPixel, int height, uint16_t *result_array)
{
	//std::cout<<__FUNCTION__<<"	"<<fname<<std::endl;
	//cout << __FUNCTION__<< " startLocation " << startLocation << " all_pixels " << all_pixels << " used_pixels " << used_pixels<< " height " << height << endl;
	int inner_used_pixel = camEndPixel - camStartPixel + 1;

	ifstream is(fname.c_str(), ios::in | ios::binary);
	if (is)
	{
		for (int h = 0; h < height; h++)
		{
			//is.seekg(startLocation + all_pixels * h * 2,ios::beg);
			is.seekg(startLocation + (all_pixels * h + camStartPixel - 1) * 2, ios::beg);
			//is.read(reinterpret_cast<char*>(result_array + used_pixels * h), used_pixels * 2);
			is.read(reinterpret_cast<char*>(result_array + inner_used_pixel * h), inner_used_pixel * 2);
		}
		is.close();
	}
	else
	{
		cerr << fname << std::endl;
		cerr << "File could not be opened!\n";
		cerr << "Error code: " << strerror(errno);
		throw invalid_argument("Data File Error");
	}
}

void Data::loadFile(std::string fname, unsigned long long startLocation, int all_pixels, int camStartPixel, int camEndPixel, int height, uint16_t *result_array)
{
	int inner_used_pixel = camEndPixel - camStartPixel + 1;

	ifstream is(fname.c_str(), ios::in | ios::binary);
	if (is)
	{
		for (int h = 0; h < height; h++)
		{
			// is.seekg(startLocation + all_pixels * h * 2,ios::beg);
			is.seekg(startLocation + (all_pixels * h + camStartPixel - 1) * 2, ios::beg);
			//is.read(reinterpret_cast<char*>(result_array + used_pixels * h), used_pixels * 2);
			is.read(reinterpret_cast<char*>(result_array + inner_used_pixel * h), inner_used_pixel * 2);
		}
		is.close();
	}
	else
	{
		cerr << fname << std::endl;
		cerr << "File could not be opened!\n";
		cerr << "Error code: " << strerror(errno);
		throw invalid_argument("Data File Error");
	}
}

void Data::loadFile(std::string fname, unsigned long long startLocation, int all_pixels, int camStartPixel, int camEndPixel, int height, float2 *result_array)
{
	int inner_used_pixel = camEndPixel - camStartPixel + 1;


	ifstream is(fname.c_str(), ios::in | ios::binary);
	if (is)
	{
		int x = 0;
		for (int h = 0; h < height; h++)
		{
			//std::cout<<"position:	"<<startLocation + all_pixels * h * 2<<std::endl;
			//is.seekg(startLocation + all_pixels * h * 2,ios::beg);
			is.seekg(startLocation + (all_pixels * h + camStartPixel - 1) * 2, ios::beg);
			for (int i = 0; i < inner_used_pixel; i++)
			{
				is.read(reinterpret_cast<char*>(&x), sizeof(uint16_t));
				//result_array[used_pixels * h + i].x = (float)x;
				result_array[inner_used_pixel * h + i].x = (float)x;
				//std::cout<<x<<"	";
			}
			//std::cout<<std::endl;
		}
		is.close();
	}
	else
	{
		cerr << fname << std::endl;
		cerr << "File could not be opened!\n"; // Report error
		cerr << "Error code: " << strerror(errno); // Get some info as to why
		throw invalid_argument("File Open Error");
	}
}