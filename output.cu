#include "output.cuh"
//#include "TiffWriter.h"
#include "utilities.h"
#include<sys/stat.h>
#include<sys/types.h>
//#include "cuda_profiler_api.h"
#include <iostream>

using namespace std;
using namespace cv;

Output::Output(Parameters &p, Data &da, Interpolation &i, Dispersion &di, Background &b) : Helper(p), data(da), interp(i), disp(di), bg(b)
{
	cout << "Data processing" << endl;
	
	height_ba = p.numAscansPerBscan/p.batchAscans;
	width_ba = p.numCameraPixels*p.batchAscans;
	width_2xba = (p.numCameraPixels*2)*p.batchAscans;
	frames_tot = p.numBScans*p.batchFrames;

	used_width_ba = used_width*p.batchAscans;
	used_width_2xba = (used_width*2)*p.batchAscans;

	dimGrid_w = dim3((width - 1) / TILE_WIDTH + 1, (height_bfr - 1)/TILE_WIDTH + 1, 1);
	dimGrid_w2 = dim3((width_2x - 1) / TILE_WIDTH + 1, (height_bfr - 1)/TILE_WIDTH + 1, 1);
	dimGrid_wt = dim3((width_trm - 1) / TILE_WIDTH + 1, (height_bfr - 1)/TILE_WIDTH + 1, 1);

	dimGrid_uw = dim3((used_width - 1) / TILE_WIDTH + 1, (height_bfr - 1)/TILE_WIDTH + 1, 1);

	dimLine_wba	= dim3((width_ba+THREADS_PER_BLOCK-1)/THREADS_PER_BLOCK, 1, 1);
	dimLine_w2xba = dim3((width_2xba+THREADS_PER_BLOCK-1)/THREADS_PER_BLOCK, 1, 1);
	dimLine_w2xba_interp = dim3((width_2xba+THREADS_PER_BLOCK-1)/THREADS_PER_BLOCK, height_ba, 1);
	dimLine_wtba = dim3(((width_trm*p.batchAscans)+THREADS_PER_BLOCK-1)/THREADS_PER_BLOCK, 1, 1);

	dimLine_uwba	= dim3((used_width_ba+THREADS_PER_BLOCK-1)/THREADS_PER_BLOCK, 1, 1);
}

Output::~Output()
{
}

void Output::process()
{
	cv::Mat prevImg;

	// Hold the full processed image stack in memory.
	float *processed_data_array = new float[width_trm * height_bfr * frames];
	std::cout<<"	width_trm:"<<width_trm<<"	height_bfr:"<<height_bfr<<"	frames:"<<frames<<std::endl;
	initResources();

	//gpuErrchk( cudaProfilerStart() );
	for (int i = 0; i < frames; i++)
	{	
		processData(i, processed_data_array);
	
		if (i%p.prevFreq == 0)
		{
			prevImg = cv::Mat(width_trm, height_1fr, CV_32F, &processed_data_array[i * width_trm * height_bfr]);
			cv::namedWindow("Preview", cv::WINDOW_AUTOSIZE);
			cv::imshow("Preview", prevImg);
			cv::waitKey(1);
		}

		// no use, maybe just for debug delete by TaoXu
		/*if (i == 10)
		{
			for( int m = 0; m < 50; m++)
			{
				for( int n = 0; n < width_trm; n++)
				{
					cout << processed_data_array[i * m * n] << " ";
				}
				cout << endl;
			}
		}*/
	}
	destroyWindow("Preview");
	freeResources();
	//gpuErrchk( cudaProfilerStop() );

	writeToDisk(processed_data_array);

	delete[] processed_data_array;
	prevImg.release(); 
}

void Output::process(std::string datatype)
{
	cudaMemGetInfo(&mem_avail, &mem_total);
	std::cout << "Debug: " << __FUNCTION__ << "0 mem avail: " << mem_avail << " total: " << mem_total << std::endl;

	cv::Mat prevImg;

	// Hold the full processed image stack in memory.
	float *processed_data_array = new float[width_trm * height_bfr * frames];
	std::cout<<"	width_trm:"<<width_trm<<"	height_bfr:"<<height_bfr<<"	frames:"<<frames<<std::endl;
	initResources();
	//std::cout<<"Debug: "<<__FUNCTION__<<" process 1"<<std::endl;
	//gpuErrchk( cudaProfilerStart() );
	for (int i = 0; i < frames; i++)
	{
		processData(i, processed_data_array);

	}
	//std::cout<<"Debug: "<<__FUNCTION__<<" process 2"<<std::endl;
	//destroyWindow("Preview"); @brian
	cudaMemGetInfo(&mem_avail, &mem_total);
	//std::cout<<"Debug: "<<__FUNCTION__<<"1 mem avail: "<<mem_avail<<" total: "<<mem_total<<std::endl;

	freeResources();
	//gpuErrchk( cudaProfilerStop() );

	// wait for change
	std::cout << __FUNCTION__ << "datatype:	"<< datatype << std::endl;
	writeToDisk(processed_data_array,datatype);

	delete[] processed_data_array;
	prevImg.release();
	//std::cout<<"Debug: "<<__FUNCTION__<<" process 2"<<std::endl;
	cudaMemGetInfo(&mem_avail, &mem_total);
	//std::cout<<"Debug: "<<__FUNCTION__<<"2 mem avail: "<<mem_avail<<" total: "<<mem_total<<std::endl;
}

void Output::initResources()
{

	//h_buff_1 = new uint16_t[height_bfr * width]();
	h_buff_1 = new uint16_t[height_bfr * used_width]();

	//gpuErrchk( cudaHostRegister(h_buff_1, (height_bfr*width*sizeof(uint16_t)), cudaHostRegisterPortable));
	gpuErrchk( cudaHostRegister(h_buff_1, (height_bfr*used_width*sizeof(uint16_t)), cudaHostRegisterPortable));
	gpuErrchk( cudaPeekAtLastError() );
	gpuErrchk( cudaStreamCreate(&stream1) );
	gpuErrchk( cudaStreamCreate(&stream2) );

	//int m[2] = {width, height_bfr};
	int m[2] = {used_width, height_bfr};
	cufftErrchk( cufftPlanMany(&plan_w,1,m,NULL,1,0,NULL,1,0,CUFFT_C2C,height_bfr) );
	cufftErrchk( cufftSetStream(plan_w,stream1) );

	int n[2] = {width_2x, height_bfr};
	cufftErrchk( cufftPlanMany(&plan_w2,1,n,NULL,1,0,NULL,1,0,CUFFT_C2C,height_bfr) );
	cufftErrchk( cufftSetStream(plan_w2,stream1) );
	
	//gpuErrchk( cudaMalloc((void **)&d_raw_data, (height_bfr * width * sizeof(uint16_t))) );
	gpuErrchk( cudaMalloc((void **)&d_raw_data, (height_bfr * used_width * sizeof(uint16_t))) );
	
	//gpuErrchk( cudaMalloc((void **)&d_proc_buff_0, (height_bfr * width * sizeof(float2))) ); // used_width
	gpuErrchk( cudaMalloc((void **)&d_proc_buff_0, (height_bfr * used_width * sizeof(float2))) );
	//gpuErrchk( cudaMalloc((void **)&d_proc_buff_1, (height_bfr * width_2x * sizeof(float2))) );
	gpuErrchk( cudaMalloc((void **)&d_proc_buff_1, (height_bfr * width_2x * sizeof(float2))) );
	gpuErrchk( cudaMalloc((void **)&d_proc_buff_2, (height_bfr * width_2x * sizeof(float2))) );
	gpuErrchk( cudaMalloc((void **)&d_proc_buff_trm, (height_bfr * width_trm * sizeof(float2))) );
	gpuErrchk( cudaMalloc((void **)&d_proc_buff_db, (height_bfr * width_trm * sizeof(float))) );
	gpuErrchk( cudaMalloc((void **)&d_proc_buff_trns, (width_trm * height_bfr * sizeof(float))) );
	
	//data.loadFile(p.fnameData, (height_1fr * width_2x * p.numBgFrame), (height_bfr*width), h_buff_1);
	//data.loadFile(p.fnameData, (height_1fr * width * 2 * p.numBgFrame), width, used_width, height_bfr, h_buff_1); //@brian
	//data.loadFile(p.fnameData, ((height_1fr * width_2x) * (p.numBgFrame+((0+1)*p.batchFrames))), width, used_width, height_bfr, h_buff_1);
	data.loadFile(p.fnameData, ((height_1fr * width_2x) * (p.numBgFrame + ((0 + 1)*p.batchFrames))), width, p.camStartPixel, p.camEndPixel, height_bfr, h_buff_1);
        //std::cout<< ((height_1fr * width_2x) * (p.numBgFrame + ((0 + 1)*p.batchFrames)))<<std::endl;
	//gpuErrchk( cudaMemcpy(d_raw_data, h_buff_1, (height_bfr * width * sizeof(uint16_t)), cudaMemcpyHostToDevice) );
	gpuErrchk( cudaMemcpy(d_raw_data, h_buff_1, (height_bfr * used_width * sizeof(uint16_t)), cudaMemcpyHostToDevice) );
}


void Output::processData(int it, float *proc_data_piece)
{
	gpuErrchk( cudaMemset(d_proc_buff_0, 0, (height_bfr * used_width * sizeof(float2))) );
	subtract<<<dimLine_uwba,dimLine_B,0,stream1>>>(height_ba, used_width_ba, d_raw_data, bg.d_bg, d_proc_buff_0); gpuErrchk( cudaPeekAtLastError() );
	
	cufftErrchk( cufftExecC2C(plan_w, d_proc_buff_0, d_proc_buff_0, CUFFT_FORWARD) );
	
	gpuErrchk( cudaMemset(d_proc_buff_1, 0, (height_bfr * width_2x * sizeof(float2))) );
	
	zero_pad<<<dimGrid_uw,dimGrid_B,0,stream1>>>(height_bfr, width, used_width, d_proc_buff_0, d_proc_buff_1); gpuErrchk( cudaPeekAtLastError() ); // 8000 -> 8192

	cufftErrchk( cufftExecC2C(plan_w2, d_proc_buff_1, d_proc_buff_1, CUFFT_INVERSE) );
	scale_IFT_x<<<dimGrid_w2,dimGrid_B,0,stream1>>>(height_bfr, width_2x, w2Recip, d_proc_buff_1); gpuErrchk( cudaPeekAtLastError() );

	interp.procInterp(height_ba, width_2xba, dimLine_w2xba, dimLine_B, stream1, d_proc_buff_1, d_proc_buff_2); // calibration?
	unsigned long long startp = (((unsigned long long)height_1fr * width_2x)) * (p.numBgFrame+((it+2)*p.batchFrames));
	data.loadFile(p.fnameData, startp, width, p.camStartPixel, p.camEndPixel, height_bfr, h_buff_1);
        //std::cout<<startp<<std::endl;

	mult_divide<<<dimLine_w2xba,dimLine_B,0,stream1>>>(height_ba, width_2xba, data.d_gauss_win, d_proc_buff_2, bg.d_bg_mask, d_proc_buff_2); gpuErrchk( cudaPeekAtLastError() );

	phi_multiply<<<dimLine_w2xba,dimLine_B,0,stream1>>>(height_ba, width_2xba, disp.d_fphi, d_proc_buff_2, d_proc_buff_1); gpuErrchk( cudaPeekAtLastError() );

	gpuErrchk( cudaMemcpyAsync(d_raw_data, h_buff_1, (height_bfr * used_width * sizeof(uint16_t)), cudaMemcpyHostToDevice, stream2) );

	cufftErrchk( cufftExecC2C(plan_w2,d_proc_buff_1, d_proc_buff_1, CUFFT_INVERSE) );


	scale_IFT<<<dimGrid_w2,dimGrid_B,0,stream1>>>(height_bfr, width_2x, w2Recip, d_proc_buff_1); gpuErrchk( cudaPeekAtLastError() );


	trim_width<<<dimGrid_wt,dimGrid_B,0,stream1>>>(height_bfr, width_2x, p.startPixel, p.endPixel, d_proc_buff_1, d_proc_buff_trm); gpuErrchk( cudaPeekAtLastError() );
	magnitude_db<<<dimGrid_wt,dimGrid_B,0,stream1>>>(height_bfr, width_trm, d_proc_buff_trm, d_proc_buff_db); gpuErrchk( cudaPeekAtLastError() );


	subt_divide<<<dimLine_wtba,dimLine_B,0,stream1>>>(height_ba, (width_trm*p.batchAscans), d_proc_buff_db, bg.d_bg_noise, grayRecip, d_proc_buff_db); gpuErrchk( cudaPeekAtLastError() );

	multiframe_transpose<<<dimGrid_wt,dimGrid_B,0,stream1>>>(height_1fr, width_trm, p.batchFrames, d_proc_buff_db, d_proc_buff_trns); gpuErrchk( cudaPeekAtLastError() );
	

	int write_offset = (it*height_bfr*width_trm);
        //std::cout<< write_offset<<std::endl;
	gpuErrchk( cudaMemcpy((proc_data_piece+write_offset), d_proc_buff_trns, (width_trm * height_bfr * sizeof(float)), cudaMemcpyDeviceToHost) );

	gpuErrchk( cudaDeviceSynchronize() );
}

void Output::writeToDisk(float *proc_data_array)
{
	Mat image;
	Mat new_image;
	char fname_opencv[200];
	string path_string;
	string r_name;
	TIFF *out;

	unsigned slash_split = p.fnameData.find_last_of("/");
	string path_name = p.fnameData.substr(0, slash_split);
	string f_name = p.fnameData.substr(slash_split + 1);
	unsigned period_split = f_name.find_last_of(".");
	//string r_name =  f_name.substr(0, period_split);
	r_name = f_name.substr(0, period_split);
	//path_string = (path_name+"\\"+r_name);
	path_string = (path_name + "/GPU_processed");
	//CreateDirectory(path_string.c_str(), NULL);
	if(mkdir(path_string.c_str(),0777)==-1)//creating a directory
	{
		cerr<<"Error :  "<<strerror(errno)<<endl;
		exit(1);
	}
	path_string = (path_string + "/" + r_name);
	//CreateDirectory(path_string.c_str(), NULL);
	if(mkdir(path_string.c_str(),0777)==-1)//creating a directory
	{
		cerr<<"Error :  "<<strerror(errno)<<endl;
		exit(1);
	}
	//tiff class using Magick++
	string tiff_path = path_string + "/" + r_name + ".tiff";
	//MagickTiff::TiffWriter tiffwriter(tiff_path);

	sprintf(fname_opencv,"%s\\%s.tiff",path_string.c_str(), r_name.c_str());
	out = TIFFOpen(fname_opencv, "w");

	//cout << "fname_opencv " << fname_opencv << endl;
	uint32 image_width, image_height;
	image_width = height_1fr;
	image_height =  width_trm;
	uint16 spp, bpp, photo, res_unit;
	spp = 1; /* Samples per pixel */
	bpp = 8; /* Bits per sample */
	photo = PHOTOMETRIC_MINISBLACK;
	float xres, yres;
	if (!p.reslice)
	//if (!p.reslice)
	//if (true)
	{
		for (int i = 0; i < frames_tot; i++)
		{
			// This conversion makes the images look like they do in MATLAB. Doing normalization, like in the
			// image class, results on low contrast images. This needs to be confirmed, but I believe all values below
			// 0 are set to 0.
			image = cv::Mat(width_trm, height_1fr, CV_32F, &proc_data_array[i*width_trm*height_1fr]);
			new_image = cv::Mat(width_trm, height_1fr, CV_8U);
			//image.convertTo(new_image,CV_8U,255);
			//image.convertTo(new_image,CV_8U,384, -64); // change the contrast @brian
			image.convertTo(new_image, CV_8U, p.contrastA, p.contrastB);
			//new_image = cv::Mat(width_trm, height_1fr, CV_16U);
			//image.convertTo(new_image,CV_16U,65535);	

			TIFFSetField(out, TIFFTAG_IMAGEWIDTH, image_width / spp);
			TIFFSetField(out, TIFFTAG_IMAGELENGTH, image_height);
			TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bpp);
			TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, spp);
			TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
			TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photo);
			TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT);
			/* It is good to set resolutions too (but it is not nesessary) */
			/*
			xres = yres = 100;
			res_unit = RESUNIT_INCH;
			TIFFSetField(out, TIFFTAG_XRESOLUTION, xres);
			TIFFSetField(out, TIFFTAG_YRESOLUTION, yres);
			TIFFSetField(out, TIFFTAG_RESOLUTIONUNIT, res_unit);
			*/

			TIFFSetField(out, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
			TIFFSetField(out, TIFFTAG_PAGENUMBER, i, frames_tot);

			for (int j = 0; j < image_height; j++)
				TIFFWriteScanline(out, &new_image.data[j * image_width], j, 0);

			TIFFWriteDirectory(out);

			/*
			if (i == 0) 
			{
				cout << "new_image data " << endl;
				for(int m=0; m < image_height; m++)
				{
					for(int n=0; n < image_width; n++)
					{
						cout << new_image.data[m * image_height + n] << " ";
					}
					cout << endl;
				}
			}
			*/
		}
	}

	TIFFClose(out);
	image.release();
	new_image.release();
	cv::waitKey(1);
}

void Output::writeToDisk(float *proc_data_array, std::string datatype)
{
	cudaMemGetInfo(&mem_avail, &mem_total);
	//std::cout<<"Debug: "<<__FUNCTION__<<"0 mem avail: "<<mem_avail<<" total: "<<mem_total<<std::endl;

	Mat image;
	Mat new_image;
	Mat avg_Bscanframe;
	char fname_opencv[200];
	string path_string;

	Mat row_mean;
	Mat avg_img;

	//use tiff
	TIFF *out;

	if (!p.reslice)
	{
		std::cout << "Debug: " << __FUNCTION__ << "frames_tot:" << frames_tot << std::endl;

		unsigned slash_split = p.fnameData.find_last_of("/");
		string path_name = p.fnameData.substr(0, slash_split);
		string f_name = p.fnameData.substr(slash_split + 1);
		unsigned period_split = f_name.find_last_of(".");
		string r_name = f_name.substr(0, period_split);
		//path_string = (path_name+"\\"+r_name);
		path_string = (path_name + "/GPU_processed");
		//CreateDirectory(path_string.c_str(), NULL);
		if (!Utility::is_file_exist(path_string.c_str()))
		{
			if(mkdir(path_string.c_str(),0777)==-1)//creating a directory
			{
				cerr<<"Error :  "<<strerror(errno)<<endl;
				exit(1);
			}
		}
		path_string = (path_string + "/" + r_name);
		//CreateDirectory(path_string.c_str(), NULL);
		if (!Utility::is_file_exist(path_string.c_str()))
		{
			if(mkdir(path_string.c_str(),0777)==-1)//creating a directory
			{
				cerr<<"Error :  "<<strerror(errno)<<endl;
				exit(1);
			}
		}

		//tiff class
		string tiff_path = path_string + "/" + r_name + ".tiff";
		//MagickTiff::TiffWriter tiffwriter(tiff_path);
		
		out = TIFFOpen(tiff_path.c_str(), "w");
		
		uint32 image_width, image_height;
		image_width = height_1fr;
		image_height =  width_trm;
		uint16 spp, bpp, photo, res_unit;
		spp = 1; /* Samples per pixel */
		bpp = 8; /* Bits per sample */
		photo = PHOTOMETRIC_MINISBLACK;
		if (datatype == "fundus")
		{
			//Revised by @Tao 10/11/2016
			sprintf(fname_opencv, "%s/0.tiff", path_string.c_str());
			// fundus view result in one image
			for (int i = 0; i < frames_tot; i++)
			{
				image = cv::Mat(width_trm, height_1fr, CV_32F, &proc_data_array[i*width_trm*height_1fr]);
				new_image = cv::Mat(width_trm, height_1fr, CV_8U);
				image.convertTo(new_image, CV_8U, 255);

				reduce(new_image, row_mean, 0, CV_REDUCE_AVG);
				avg_img.push_back(row_mean);
			}
			imwrite(fname_opencv, avg_img);
		}
		else {
			// process the other data type
			int avg_inter = 0;
			int avgBscan = p.avgBscan;
			avg_Bscanframe = Mat::zeros(width_trm, height_1fr, CV_32F);

			//Revised by @Tao 10/11/2016
			/*
			in order to write to disk in the form of TIFF
			*/

			std::cout << "start writing to disk!" << std::endl;
			for (int i = 0; i < frames_tot; i++)
			{
				// This conversion makes the images look like they do in MATLAB. Doing normalization, like in the
				// image class, results on low contrast images. This needs to be confirmed, but I believe all values below 0 are set to 0.

				//std::cout<<"Debug: "<<__FUNCTION__<<"frames_tot: "<<i<< " avg_inter:" << avg_inter << std::endl;
				image = cv::Mat(width_trm, height_1fr, CV_32F, &proc_data_array[i*width_trm*height_1fr]);

				//cout << "image = "<< endl << " "  << image << endl << endl;

				avg_Bscanframe += image * (1.0 / avgBscan);
				/*
				new_image = cv::Mat(width_trm, height_1fr, CV_8U);
				image.convertTo(new_image,CV_8U,255);
				*/

				//cout << "output to disk 0 "<< path_string << endl;

				if (avg_inter == avgBscan - 1){
					new_image = cv::Mat(width_trm, height_1fr, CV_8U);
					//revised by Tao 11/17/2016, adjust the contrast
					//avg_Bscanframe.convertTo(new_image,CV_8U,255);
					avg_Bscanframe.convertTo(new_image, CV_8U, this->p.contrastA, this->p.contrastB);
					//sprintf(fname_opencv,"%s\\%i.png",path_string.c_str(),i/avgBscan);

					//add by Tao 12012016
					//tiffwriter.write(new_image.data, new_image.rows, new_image.cols);
					TIFFSetField(out, TIFFTAG_IMAGEWIDTH, image_width / spp);
					TIFFSetField(out, TIFFTAG_IMAGELENGTH, image_height);
					TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bpp);
					TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, spp);
					TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
					TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photo);
					TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT);

					TIFFSetField(out, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
					TIFFSetField(out, TIFFTAG_PAGENUMBER, i, frames_tot);

					for (int j = 0; j < image_height; j++)
						TIFFWriteScanline(out, &new_image.data[j * image_width], j, 0);

					TIFFWriteDirectory(out);
					//remove by Tao 12012016
					//imwrite(fname_opencv, new_image);
					//cout << "output to disk "<< fname_opencv<< endl;

					avg_Bscanframe = Mat::zeros(width_trm, height_1fr, CV_32F);
				}


				avg_inter = (avg_inter + 1) % avgBscan;

			}
			//Magick::writeImages(tiffwriter.begin(), tiffwriter.end(), tiffwriter.get_filename());
			//tiffwriter.Destroy();
			std::cout << "write to tiff finished!" << endl;
			TIFFClose(out);
		}
		/*
		for (int i = 0; i < frames_tot; i++)
		{

		// This conversion makes the images look like they do in MATLAB. Doing normalization, like in the
		// image class, results on low contrast images. This needs to be confirmed, but I believe all values below
		// 0 are set to 0.
		image = cv::Mat(width_trm, height_1fr, CV_32F, &proc_data_array[i*width_trm*height_1fr]);
		new_image = cv::Mat(width_trm, height_1fr, CV_8U);
		image.convertTo(new_image,CV_8U,255);
		//new_image = cv::Mat(width_trm, height_1fr, CV_16U);
		//image.convertTo(new_image,CV_16U,65535);

		reduce(new_image,row_mean, 0, CV_REDUCE_AVG);
		avg_img.push_back(row_mean);
		// Try to create the directory on the first iteration.
		if (i == 0)
		{
		unsigned slash_split = p.fnameData.find_last_of("/\\");
		string path_name = p.fnameData.substr(0,slash_split);
		string f_name = p.fnameData.substr(slash_split+1);
		unsigned period_split = f_name.find_last_of(".");
		string r_name =  f_name.substr(0, period_split);
		//path_string = (path_name+"\\"+r_name);
		path_string = (path_name+"\\GPU_processed");
		CreateDirectory(path_string.c_str(),NULL);
		path_string = (path_string + "\\" + r_name);
		CreateDirectory(path_string.c_str(),NULL);
		}
		//std::cout<<"Debug: "<<__FUNCTION__<<"path_string:"<<path_string<<std::endl;
		//sprintf(fname_opencv,"%s\\%i.tiff",path_string.c_str(),i);
		sprintf(fname_opencv,"%s\\%i.png",path_string.c_str(),i);
		imwrite(fname_opencv, new_image);
		}
		imwrite("E:\\a.png", avg_img);
		*/
	}
	else
	{// reslice before saving to disk
		float *resliced_data_array = new float[height_bfr*width_trm*frames];

		for (int i = 0; i < width_trm; i++)
		for (int j = 0; j < frames_tot; j++)
		for (int k = 0; k < height_1fr; k++)
			resliced_data_array[i*frames_tot*height_1fr + j*height_1fr + k] =
			proc_data_array[j*width_trm*height_1fr + i*height_1fr + k];

		for (int i = 0; i < width_trm; i++)
		{
			image = Mat(frames_tot, height_1fr, CV_32F, &resliced_data_array[i*frames_tot*height_1fr]);
			new_image = Mat(frames_tot, height_1fr, CV_8U);
			image.convertTo(new_image, CV_8U, 255);
			//new_image = cv::Mat(width_trm, height_1fr, CV_16U);
			//image.convertTo(new_image,CV_16U,65535);

			cv::namedWindow("Preview", cv::WINDOW_AUTOSIZE);
			cv::imshow("Preview", new_image);
			cv::waitKey(1);

			// Try to create the directory on the first iteration.
			if (i == 0)
			{
				unsigned slash_split = p.fnameData.find_last_of("/");
				string path_name = p.fnameData.substr(0, slash_split);
				string f_name = p.fnameData.substr(slash_split + 1);
				unsigned period_split = f_name.find_last_of(".");
				string r_name = f_name.substr(0, period_split);
				//string path_string = (path_name+"\\"+r_name+" - reslice");

				path_string = (path_name + "/GPU_processed");
				//CreateDirectory(path_string.c_str(), NULL);
				if(mkdir(path_string.c_str(),0777)==-1)//creating a directory
				{
					cerr<<"Error :  "<<strerror(errno)<<endl;
					exit(1);
				}
				path_string = (path_string + "/" + r_name + " - reslice");
				//CreateDirectory(path_string.c_str(), NULL);
				if(mkdir(path_string.c_str(),0777)==-1)//creating a directory
				{
					cerr<<"Error :  "<<strerror(errno)<<endl;
					exit(1);
				}
				
			}

			sprintf(fname_opencv, "%s/%i.tiff", path_string.c_str(), i);
			imwrite(fname_opencv, new_image);
		}
		destroyWindow("Preview");
		delete[] resliced_data_array;
	}
	image.release();
	new_image.release();
	avg_Bscanframe.release();
	cv::waitKey(1);
	cudaMemGetInfo(&mem_avail, &mem_total);
	//std::cout<<"Debug: "<<__FUNCTION__<<"1 mem avail: "<<mem_avail<<" total: "<<mem_total<<std::endl;
	/*std::cout<<"contrastA:	"<<this->p.contrastA<<std::endl;
	std::cout<<"contrastB:	"<<this->p.contrastB<<std::endl;
	std::cout<<"alpha:	"<<this->p.alpha<<std::endl;*/
}

/*
void Output::writeToDisk(float *proc_data_array)
{
	Mat image;
	Mat new_image;
	char fname_opencv[200];
	string path_string; 
	string r_name;
	if (!p.reslice)
	{
		for (int i = 0; i < frames_tot; i++)
		{
			// This conversion makes the images look like they do in MATLAB. Doing normalization, like in the
			// image class, results on low contrast images. This needs to be confirmed, but I believe all values below
			// 0 are set to 0.
			image = cv::Mat(width_trm, height_1fr, CV_32F, &proc_data_array[i*width_trm*height_1fr]);
			new_image = cv::Mat(width_trm, height_1fr, CV_8U);
			image.convertTo(new_image,CV_8U,255);
			//new_image = cv::Mat(width_trm, height_1fr, CV_16U);
			//image.convertTo(new_image,CV_16U,65535);	

			// Try to create the directory on the first iteration.
			if (i == 0) 
			{
				unsigned slash_split = p.fnameData.find_last_of("/\\");
				string path_name = p.fnameData.substr(0,slash_split);
				string f_name = p.fnameData.substr(slash_split+1);
				unsigned period_split = f_name.find_last_of("."); 
				//string r_name =  f_name.substr(0, period_split);
				r_name =  f_name.substr(0, period_split);
				//path_string = (path_name+"\\"+r_name);
				path_string = (path_name+"\\GPU_processed");
				CreateDirectory(path_string.c_str(),NULL);
				path_string = (path_string + "\\" + r_name);
				CreateDirectory(path_string.c_str(),NULL);
			}
			sprintf(fname_opencv,"%s\\%s_%i.tiff",path_string.c_str(), r_name.c_str(), i);
			imwrite(fname_opencv, new_image);
		}
	}

	// reslice before saving to disk
	else
	{
		float *resliced_data_array = new float[height_bfr*width_trm*frames];

		for (int i = 0; i < width_trm; i++)
			for (int j = 0; j < frames_tot; j++) 
				for (int k = 0; k < height_1fr; k++)
					resliced_data_array[i*frames_tot*height_1fr+j*height_1fr+k] = 
						proc_data_array[j*width_trm*height_1fr+i*height_1fr+k];

		for (int i = 0; i < width_trm; i ++)	
		{
			image = Mat(frames_tot, height_1fr, CV_32F, &resliced_data_array[i*frames_tot*height_1fr]);
			new_image = Mat(frames_tot, height_1fr, CV_8U);
			image.convertTo(new_image, CV_8U, 255);
			//new_image = cv::Mat(width_trm, height_1fr, CV_16U);
			//image.convertTo(new_image,CV_16U,65535);

			cv::namedWindow("Preview", cv::WINDOW_AUTOSIZE);
			cv::imshow("Preview", new_image);			
			cv::waitKey(1);
			
			// Try to create the directory on the first iteration.
			if (i == 0)
			{
				unsigned slash_split = p.fnameData.find_last_of("/\\");
				string path_name = p.fnameData.substr(0,slash_split);
				string f_name = p.fnameData.substr(slash_split+1);
				unsigned period_split = f_name.find_last_of("."); 
				string r_name =  f_name.substr(0, period_split);
				//string path_string = (path_name+"\\"+r_name+" - reslice");

				path_string = (path_name+"\\GPU_processed");
				CreateDirectory(path_string.c_str(),NULL);
				path_string = (path_string + "\\" + r_name + " - reslice");
				CreateDirectory(path_string.c_str(),NULL);
			}

			sprintf(fname_opencv,"%s\\%i.tiff",path_string.c_str(),i);
			imwrite(fname_opencv, new_image);
		}
		destroyWindow("Preview");
		delete[] resliced_data_array;
	}
	image.release();
	new_image.release();
	cv::waitKey(1);
}
*/
void Output::freeResources()
{
	cudaMemGetInfo(&mem_avail, &mem_total);
	//std::cout<<"Debug: "<<__FUNCTION__<<"-1 mem avail: "<<mem_avail<<" total: "<<mem_total<<std::endl;

	// modify here for cuda error
	cufftDestroy(plan_w);
	cufftDestroy(plan_w2);

	cudaMemGetInfo(&mem_avail, &mem_total);
	//std::cout<<"Debug: "<<__FUNCTION__<<"0 mem avail: "<<mem_avail<<" total: "<<mem_total<<std::endl;

	gpuErrchk(cudaStreamDestroy(stream1));
	gpuErrchk(cudaStreamDestroy(stream2));

	cudaMemGetInfo(&mem_avail, &mem_total);
	//std::cout<<"Debug: "<<__FUNCTION__<<"1 mem avail: "<<mem_avail<<" total: "<<mem_total<<std::endl;

	gpuErrchk(cudaFree(d_raw_data));
	gpuErrchk(cudaFree(d_proc_buff_0));
	gpuErrchk(cudaFree(d_proc_buff_trm));
	gpuErrchk(cudaFree(d_proc_buff_db));
	gpuErrchk(cudaFree(d_proc_buff_trns));
	gpuErrchk(cudaFree(d_proc_buff_1));
	gpuErrchk(cudaFree(d_proc_buff_2));

	cudaMemGetInfo(&mem_avail, &mem_total);
	//std::cout<<"Debug: "<<__FUNCTION__<<"2 mem avail: "<<mem_avail<<" total: "<<mem_total<<std::endl;

	gpuErrchk(cudaHostUnregister(h_buff_1));
	delete[] h_buff_1;

	cudaMemGetInfo(&mem_avail, &mem_total);
}
