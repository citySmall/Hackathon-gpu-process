objects := kernel.o image.o gpu_kernels.o helper.o parameters.o data.o \
			interpolation.o dispersion.o background.o output.o \
			prcssr.o ProcessorWrapper.o ProcessorApi.o

CUDA_INSTALL_PATH := /usr/local/cuda

CXX := g++
NVCC := nvcc

# Includes
INCLUDES = -I.
OPENCV_INCLUDES = `pkg-config --cflags opencv`
# MAGICK_INCLUDES = -I/usr/include/GraphicsMagick
CUDA_INCLUDES = -I$(CUDA_INSTALL_PATH)/include
COMMON_INCLUDES = $(INCLUDES) $(OPENCV_INCLUDES) $(CUDA_INCLUDES)

# libraries
# MAGICK_LIB = -L/usr/lib -L/usr/lib/X11 -lGraphicsMagick++ -lGraphicsMagick -ljbig -llcms2 -ltiff -lfreetype -ljasper -ljpeg -lpng12 -lwmflite -lXext -lSM -lICE -lX11 -llzma -lbz2 -lxml2 -lz -lm -lgomp -lpthread
LIB_CUDA = -L$(CUDA_INSTALL_PATH)/lib64 -lcudart -lcufft
THRID_PARTY = `pkg-config --libs opencv` -ltiff


GPU_process : $(objects)
				$(CXX) -o GPU_process $(objects) $(LIB_CUDA) $(THRID_PARTY)
kernel.o : kernel.cu
				$(NVCC) -c kernel.cu $(INCLUDES) $(CUDA_INCLUDES)
ProcessorApi.o : ProcessorApi.cpp
				$(CXX) -c ProcessorApi.cpp $(INCLUDES) $(CUDA_INCLUDES)
ProcessorWrapper.o : ProcessorWrapper.cpp
				$(CXX) -c ProcessorWrapper.cpp $(INCLUDES) $(CUDA_INCLUDES)
prcssr.o : prcssr.cpp
				$(CXX) -c prcssr.cpp $(INCLUDES) $(CUDA_INCLUDES)
output.o : output.cu
				$(NVCC) -c output.cu $(INCLUDES) $(CUDA_INCLUDES)
background.o : background.cu
				$(NVCC) -c background.cu $(INCLUDES) $(CUDA_INCLUDES)
dispersion.o : dispersion.cu
				$(NVCC) -c dispersion.cu $(INCLUDES) $(CUDA_INCLUDES)
image.o : image.cpp
				$(CXX) -c image.cpp $(INCLUDES) $(OPENCV_INCLUDES)
interpolation.o : interpolation.cu
				$(NVCC) -c interpolation.cu $(INCLUDES) $(CUDA_INCLUDES)
data.o : data.cu
				$(NVCC) -c data.cu $(INCLUDES) $(CUDA_INCLUDES)
helper.o : helper.cu
				$(NVCC) -c helper.cu $(INCLUDES) $(CUDA_INCLUDES)
gpu_kernels.o :  gpu_kernels.cu
				$(NVCC) -c gpu_kernels.cu $(INCLUDES) $(CUDA_INCLUDES)
parameters.o :  parameters.cpp
				$(CXX) -c parameters.cpp $(INCLUDES)
.PHONY : clean
clean :
				rm GPU_process $(objects)