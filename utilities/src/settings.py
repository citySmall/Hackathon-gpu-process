#!/usr/intel/pkgs/python/2.7.5/bin/python2.7 -OO
# Python 2.7

# NAME: settings.py
# DATE: Sept 2010
# AUTH: Sean Xiaokang Shi
# DESC: 

# from __future__ import print_function
#   http://docs.python.org/whatsnew/2.6.html
#   def print(*args, sep=' ', end='\n', file=None)
#   The parameters are:
#    	* args: positional arguments whose values will be printed out.
#   * sep: the separator, which will be printed between arguments.
#   * end: the ending text, which will be printed after all of the arguments have been output.
# 	* file: the file object to which the output will be sent.

import os
import sys
import time
# import re
# import commands
import shlex
import subprocess as sp

import compileall
compileall.compile_dir(dir=os.path.dirname(os.path.normpath(__file__)),
                       maxlevels=1, force=False, quiet=True)


def get_day_of_week(date_string):
    # day of week (Monday = 0) of a given month/day/year
    t1 = time.strptime(date_string, "%m/%d/%Y")
    # year in time_struct t1 can not go below 1970 (start of epoch)!
    t2 = time.mktime(t1)
    return time.localtime(t2)[6]
    '''
    Weekday = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    # more exactly 01/01/1970 at 0 UT (midnight Greenwich, England)
    print "11/12/1970 was a", Weekday[getDayOfWeek("11/12/1970")]
    '''


pD = lambda my_str: print('-D-', time.strftime("%H:%M", time.localtime()), my_str)
"""
pD is a lambda function for debuging.
to get second #
    pD = lambda str : print('-D-', time.strftime("%H:%M:%S", time.localtime()), str)
"""
# pD = lambda str: print('-D-', str); print('-T-', '\n')
#   def pD(str) :
#     	print('-D-', str, '\n')


class Log:
    def __init__(self, logfile_name="py.log"):
        if os.getenv('WARD'):
            if os.path.exists(os.path.join(os.getenv('WARD'), 'py', 'log')):
                logfile_path = os.path.join(os.getenv('WARD'), 'py', 'log')
            else:
                logfile_path = os.getenv('WARD')
        else:
            logfile_path = os.getcwd()
        # pD('log file name is: ' + logfile_path + '/' + logfile_name)
        try:
            self.file = open(logfile_path + '/' + logfile_name, 'w')
        except Exception as inst:
            print('-W-', str(inst))
            try:
                self.file = open('/tmp/' + logfile_name, 'w')
            except Exception as inst:
                print('-I-', str(inst))
                print('-F-', 'cannot write log file: ' + logfile_path + '/' + logfile_name)
                sys.exit()


log = Log()


def pI(str_line):
    """
    pI is a function for INFORMATION log, including screen displaying and file writing
    """
    print('-I-', str_line)
    print('-I-', str_line, file=log.file)


def pW(str_line):
    """
    pW is a function for Warning log, including screen displaying and file writing
    """
    print('-W-', str_line)
    print('-W-', str_line, file=log.file)


def pE(str_line):
    # pE is a function for ERROR log, including screen displaying and file writing
    print('-E-', str_line)
    print('-E-', str_line, file=log.file)
	
def pF(str_line):
    # pF is a function for Fetal error log, including screen displaying and file writing
    print('-F-', str_line)
    print('-F-', str_line, file=log.file)
    sys.exit(1)


def cmd(cmd_line, rundir = '', logpath = '', to_run_message = '', success_message = '', failure_message= '', os_error_message = '', sys_call = 'sp.call') : 
    if rundir:
        # pD("the rundir is : " + rundir)
        pre_dir = os.curdir
    if not os.path.exists(rundir):
        os.makedirs(rundir)
    os.chdir(rundir)
    if to_run_message:
        pI(to_run_message)
    else:
        pI("running: " + cmd_line)
    if sys_call == 'sp.call' : 
        args = shlex.split(cmd_line)
        pD(args)
        try:
            r = sp.call(args)
            if r < 0:
                if failure_message :
                    pF(failure_message + " : " + r)
                else:
                    pF( "Child was terminated by signal :" + r )
            else:
                if success_message:
                    pI(success_message)
        except:
            pF( sys.exc_info() )
        '''
        except OSError, e:
            if os_error_message :
                pE(os_error_message)
                pF(cmd_line + '\n\tExecution failed: ' + str(e) )
        '''

    elif sys_call == 'sp.check_output':
        args = shlex.split(cmd_line)
        pD(args)
        try:
            r = sp.check_output(args)
            if r < 0:
                if failure_message :
                    pF(failure_message + " : " + r)
                else:
                    pF( "Child was terminated by signal :" + r )
            else:
                if success_message :
                    pI(success_message)
        except:
            pF( sys.exc_info() )
        '''        
        except OSError, e:
            if os_error_message :
                pE(os_error_message)
                pF(cmd_line + '\n\tExecution failed: ' + str(e) )
        '''
    elif sys_call == 'os.system':
        try:
            r = os.system(cmd_line)
            if r < 0 :
                pF( 'cmd ' + cmd_line + ' was terminated with signal : ' + r)
        except:
            pF(cmd_line + '\n\t Execution failed; ' + sys.exc_info() )
    else:
        pF('-settings- sys_call "' + sys_call + '" is not supported.')

    if rundir:
        os.chdir(pre_dir)
    return r


def link2pathes(
        src, dst, overwrite=1, require_src_exist=1, error='F'):
    pI('linking 2 files, \n\tsrc = ' + src + \
       '\n\tdst = ' + dst)
    if require_src_exist:
        if not os.path.exists(src):
            pE('cannot find source path: ' + src)
            if error == 'F':
                pF('need an existing source fie: ' + src)
            elif error == 'E':
                pE('need an existing source fie: ' + src)
            else:
                pW('need an existing source fie: ' + src)

    if os.path.exists(dst):
        if not overwrite:
            pW('the destination path has exists: ' + dst)
            return 0
        else:
            pW('removing existing destination path: ' + dst)
            if os.path.isfile(dst):
                os.remove(dst)
            else:
                os.removedirs(dst)

    try:
        os.symlink(src, dst)
        pI('linked ' + src + ' to ' + dst)
        return 1
    except:
        pE('failed to link ' + src + ' to ' + dst)
        if error == 'F':
            pF(sys.exc_info())
        elif error == 'E':
            pE(sys.exc_info())
        else:
            pW(sys.exc_info())

        return 0

'''
def echo_line(line):
    l_ue = 'echo "' + line.strip() + '"'
    try:
        lout = commands.getoutput(l_ue)
        pI(lout)
        return lout
    except:
        pE('failed to convert in UE: ' + line + '\n\twith: ' + l_ue)
        pF(sys.exc_info())

'''