#! /usr/bin/python3.6


# Copyright (c) 2017 by Sean Xiaokang Shi. All rights reserved.
#
# This program is to identify the difference between
# 2 image files in a quantitative way.

from settings import pF
import argparse
parser = argparse.ArgumentParser(description='program to numerically compare output between 2 .tiff files')

parser.add_argument(
    '-t1', '--tiff_file_1',
    required=True,
    nargs=1,
    metavar='t1',
    # usage='duplicate a WARD for T2R run by copying files in t2r_input_file setting.',
    # description='description',
    help='used to specify the tiff file #1')

parser.add_argument(
    '-t2', '--tiff_file_2',
    required=True,
    nargs=1,
    metavar='t2',
    # usage='duplicate a WARD for T2R run by copying files in t2r_input_file setting.',
    # description='description',
    help='used to specify the tiff file #2')

parser.add_argument(
    '-p', '--percentage_threshold',
    required=True,
    nargs=1,
    metavar='pt',
    # usage='duplicate a WARD for T2R run by copying files in t2r_input_file setting.',
    # description='description',
    help='used to specify the threshold for percentage diff')


def tiffDiff(show=0):
    args = parser.parse_args()

    from PIL import Image
    tiff_file_1 = args.tiff_file_1[0]
    import numpy

    try:
        with Image.open(tiff_file_1) as img1:
            if show:
                img1.show()
            img_array1 = numpy.array(img1)
            img_sum1 = img_array1.sum()

            tiff_file_2 = args.tiff_file_2[0]
            with Image.open(tiff_file_2) as img2:
                if show:
                    img2.show()
                img_array2 = numpy.array(img2)
                img_sum2 = img_array2.sum()

                print((img_sum1 - img_sum2) * 200.0 / (img_sum1 + img_sum2))
                from my_math import str2float
                percentage_threshold = str2float(args.percentage_threshold[0])
                if abs(img_sum1 - img_sum2) <= (float(img_sum1 + img_sum2) * percentage_threshold / 200.0):
                    return 0
                else:
                    return 1
    except:
        pF('Errors')
        return 1

if __name__ == '__main__':
    if tiffDiff():
        exit(1)
    else:
        exit()
