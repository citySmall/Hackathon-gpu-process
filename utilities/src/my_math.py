#!/usr/intel/pkgs/python/2.7/bin/python2.7 -OO
# Python 2.7

# NAME: my_math.py
# DATE: May 2011
# AUTH: Sean Xiaokang Shi
# DESC:  

from settings import pD, pI, pW, pE, pF

from decimal import *

def str2float (str) :
	while True:
		str = str.replace(',', '')
		try :
			# try int
			num = int(str)
		except ValueError:
			try :
				# try float
				num = float(str)
			except ValueError:
				# try percentage
				try :
					if str.endswith('%') :
						num = float(str.rstrip('%'))/100
				except ValueError:
					pF('cannot handle : ' + str)
		return num
	# def str2float (str) :

def is_float ( str ) :
	try :
		float(str)
		return True
	except ValueError :
		return False

	# def is_float ( str ) :
