
#include <iostream>
#include <string>
#include <fstream>
#include "ProcessorCalibration.h"
#include "ProcessorApi.h"
#include <time.h>

using namespace std;

int batchFrames = 1;					// batch frames. Lowers preview frequency. Lower numbers are fine ~2.
int batchAscans = 20;					// batch A-scans to increase occupancy on GPU. 10 or 20 gets decent results.
int prevFreq = 40;						// 1 means show every frame, 2 means show every other frame, 4 means show 1 in 4 frames, etc. Higher is better, has an decent impact on speed.
bool reslice = false;						// reslice into enface view								
// Image settings. Same as MATLAB.

int numAscansPerBscan = batchFrames * 600; 
int numBScans = 600 / batchFrames;
int avgBscan = 1;
int numCameraPixels = 4096;

int numUsedPixels = 4000;
int cameraStartPixel = 1;
int cameraEndPixel = 4000;

int numBgFrame = 2;
int startPixel = 200;
int endPixel = 1100;

int contrastA = 384;
int contrastB = -64;
float alpha = 0.5;


int grayLevel = 50;
bool interpLinear = true;

bool dispModeGS = false;
int dispFrame = 400;
int dispTests = 100;
float dispA2 = 0.0878787;
float dispA3 = -2.34276e-06;


bool is_file_exist(const char *fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}

int main(int argc, char **argv)
{
	std::string fnamePhase = "./4096_calibration_file.txt";
	std::string fnameBg;
	std::string fnameData;
	std::string fnameData2;
	if (argc == 1)
	{
		fnameBg = "../C60/test00.bin";
		fnameData = "../C60/test01.bin";
		fnameData2 = "../C60/test01.bin";
	}
	else if (argc == 3)
	{
		fnameBg = string(argv[1]);
		fnameData = string(argv[2]);
		fnameData2 = string(argv[2]);
	} else if (argc == 4)
	{
		fnameBg = string(argv[1]);
		fnameData = string(argv[2]);
		fnameData2 = string(argv[3]);
	} else
	{
		std::cerr << "argument error"<<std::endl;
		exit(0);
	}
	time_t now = time(0);
	tm *ltm = localtime(&now);
	std::cout<<1+ltm->tm_hour<<":"<<1+ltm->tm_min<<":"<<1 + ltm->tm_sec<<"\t"<<std::endl;
	ProcessorApi* prcsrApi = new ProcessorApi();
	prcsrApi->setProcessorCalibration(batchFrames, batchAscans, prevFreq, reslice,
		numAscansPerBscan, numBScans, avgBscan, numCameraPixels, numBgFrame,
		cameraStartPixel, cameraEndPixel, numUsedPixels,
		startPixel, endPixel, grayLevel, interpLinear,
		dispModeGS, dispFrame, dispTests, dispA2, dispA3,
		fnamePhase, fnameBg, fnameData,
		contrastA, contrastB);

	prcsrApi->processBg(dispModeGS, dispA2, dispA3);
	std::cout << "a2:	"<<dispA2<<std::endl;
	std::cout << "a3:	"<<dispA3<<std::endl; 
	prcsrApi->processFrameData(fnameData2, "test");
	now = time(0);
	ltm = localtime(&now);
	std::cout<<1+ltm->tm_hour<<":"<<1+ltm->tm_min<<":"<<1 + ltm->tm_sec<<"\t"<<std::endl;
	delete prcsrApi;
	return 0;
}
